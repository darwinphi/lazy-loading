// document.addEventListener("DOMContentLoaded", function (event) {
	let head = document.querySelector("head")

	let loadJS = function (src) {
		let jsLink = document.createElement("script")
		jsLink.src = src
		head.appendChild(jsLink)
	}

	console.log("First JS file loaded")

	const app = document.querySelector("[data-js=app]")
	app.addEventListener("click", () => {
		loadJS("app.js")
	})
// })